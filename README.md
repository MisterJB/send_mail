# **Requirement**
The smtplib is part of python's standard library, so you do not have to install it, but you have to install the datetime library.
```
pip install DateTime
```

# **Config**

## email_address
Enter the sender mail address here.

For example:
```
"admin@gmail.com"
```

## smtp_server
Enter the email provider smtp server here.

For example:
```
"smtp.gmail.com"
```

## smtp_port
Enter the email provider smtp port here.

For example:
```
"465"
```

## email_password
Enter the email password here.

For example:
```
"super_secure"
```

## email_recipient_1
Enter the first email recipient here.

For example:
```
"yzmc@mailed.ro"
```

## email_recipient_2
Enter the second email recipient here.

For example:
```
"baoc@kusrc.com"
```

## email_subject
Enter the email subject here.

For example:
```
"An extremely exciting subject"
```

## email_message
Enter the email message here, note that line breaks are marked with an `\n`.

For example:
```
"Hi,\n\nthat's a test mail\n\nPS: by the way an important info\n\ngreetings\nSome Human"
```

# **Run the App**
Just run this command.
```
python mail.py
```
On top, you can also run this app with a scheduler (cron)
