import smtplib
import datetime
import config


def logging(msg):
    with open("email_log.txt", "a+") as text_file:
        text_file.write(
            "{:%Y-%m-%d %H:%M:%S} | ".format(datetime.datetime.now()))
        text_file.write("Error: %s \n" % msg)


def sendemail(from_addr, to_addr_list,
              subject, message,
              login, password,
              smtpserver):
    try:
        header = "From: %s\n" % from_addr
        header += "To: %s\n" % ",".join(to_addr_list)
        header += "Subject: %s\n\n" % subject
        message = header + message
        server = smtplib.SMTP(smtpserver)
        server.starttls()
        server.login(login, password)
        problems = server.sendmail(from_addr, to_addr_list, message)
        server.quit()
        logging("%s" % problems)
    except:
        logging("Something went wrong")


sendemail(from_addr=config.email_address,
          to_addr_list=[config.email_recipient_1, config.email_recipient_2],
          subject=config.email_subject,
          message=config.email_message,
          login=config.email_address,
          password=config.email_password,
          smtpserver=config.smtp_server+":"+config.smtp_port)
